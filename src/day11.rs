//! Day 11: Seating System

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Cell {
    Floor,
    Empty,
    Occupied,
}

type Grid = Vec<Vec<Cell>>;

#[aoc_generator(day11)]
fn parse_input(input: &str) -> Grid {
    input
        .lines()
        .map(|l| {
            l.bytes()
                .map(|b| match b {
                    b'.' => Cell::Floor,
                    b'L' => Cell::Empty,
                    b'#' => Cell::Occupied,
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect()
}

#[aoc(day11, part1)]
fn solve_part1(input: &Grid) -> usize {
    final_occupied_count(&input, false, 4)
}

#[aoc(day11, part2)]
fn solve_part2(input: &Grid) -> usize {
    final_occupied_count(&input, true, 5)
}

fn final_occupied_count(grid: &Grid, is_ranged: bool, n: usize) -> usize {
    let it1 = evolutions(&grid, is_ranged, n);
    let it2 = evolutions(&grid, is_ranged, n).skip(1);
    let mut zip = it1.zip(it2).skip_while(|(grid1, grid2)| grid1 != grid2);

    zip.next()
        .unwrap()
        .0
        .iter()
        .flatten()
        .filter(|&&cell| cell == Cell::Occupied)
        .count()
}

fn evolutions(
    grid: &Grid,
    is_ranged: bool,
    n: usize,
) -> impl Iterator<Item = Grid> {
    std::iter::successors(Some(grid.clone()), move |grid| {
        Some(evolve(grid, is_ranged, n))
    })
}

fn evolve(grid: &Grid, is_ranged: bool, n: usize) -> Grid {
    let mut new_grid = grid.clone();

    for row in 0..grid.len() {
        for column in 0..grid[0].len() {
            let count = adj_count(&grid, row, column, is_ranged);
            if grid[row][column] == Cell::Empty && count == 0 {
                new_grid[row][column] = Cell::Occupied;
            } else if grid[row][column] == Cell::Occupied && count >= n {
                new_grid[row][column] = Cell::Empty;
            }
        }
    }

    new_grid
}

fn adj_count(grid: &Grid, row: usize, column: usize, is_ranged: bool) -> usize {
    let w = grid[0].len() as i32;
    let h = grid.len() as i32;
    let mut result = 0;
    for dx in -1..=1 {
        for dy in -1..=1 {
            if dx == 0 && dy == 0 {
                continue;
            }
            let mut x = column as i32;
            let mut y = row as i32;
            loop {
                x += dx;
                y += dy;
                if x < 0 || x >= w || y < 0 || y >= h {
                    break;
                }
                match grid[y as usize][x as usize] {
                    Cell::Occupied => {
                        result += 1;
                        break;
                    }
                    Cell::Empty => {
                        break;
                    }
                    _ => {}
                }
                if !is_ranged {
                    break;
                }
            }
        }
    }
    result
}

#[allow(dead_code)]
fn display_grid(grid: &Grid) {
    for row in 0..grid.len() {
        for column in 0..grid[0].len() {
            let ch = match grid[row][column] {
                Cell::Floor => '.',
                Cell::Empty => 'L',
                Cell::Occupied => '#',
            };
            print!("{}", ch);
        }
        println!();
    }
}
