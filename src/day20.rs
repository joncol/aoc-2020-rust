//! Day 20: Jurassic Jigsaw

use std::collections::HashSet;

const TILE_SIZE: usize = 10;
const N: usize = 12;

#[derive(Debug, Clone)]
struct Tile(Vec<Vec<u8>>);

impl Tile {
    fn without_border(&self, flip_rot: FlipRot) -> Vec<Vec<u8>> {
        transform(
            &self
                .0
                .iter()
                .skip(1)
                .take(TILE_SIZE - 2)
                .map(|row| {
                    row.iter()
                        .copied()
                        .skip(1)
                        .take(TILE_SIZE - 2)
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>(),
            flip_rot,
        )
    }
}

// Rotate and flip grid.
fn transform<T>(grid: &Vec<Vec<T>>, flip_rot: FlipRot) -> Vec<Vec<T>>
where
    T: Clone,
{
    let mut result = rotate(grid, flip_rot.rotation);

    if flip_rot.flip {
        for row in &mut result {
            row.reverse();
        }
    }

    result
}

// Rotates the grid the specified number of quarter-turns counter-clockwise, by
// first transposing it, then reversing the row order.
fn rotate<T>(grid: &Vec<Vec<T>>, rotation: u8) -> Vec<Vec<T>>
where
    T: Clone,
{
    let mut result = grid.clone();
    for _ in 0..rotation.rem_euclid(4) {
        result = transpose(&result);
        result.reverse();
    }
    result
}

fn transpose<T>(grid: &Vec<Vec<T>>) -> Vec<Vec<T>>
where
    T: Clone,
{
    assert!(!grid.is_empty());
    (0..grid[0].len())
        .map(|i| {
            grid.iter()
                .map(|inner| inner[i].clone())
                .collect::<Vec<T>>()
        })
        .collect()
}

#[derive(Debug, Default, Copy, Clone)]
struct FlipRot {
    rotation: u8, // 0, 1, 2, 3 counter-clockwise 90-degree rotations.
    flip: bool,
}

#[derive(Debug)]
enum BorderLocation {
    Top,
    Left,
    Bottom,
    Right,
}

impl From<u8> for FlipRot {
    fn from(bits: u8) -> Self {
        let rotation = bits & 0x03;
        let flip = (bits & 0x04) != 0;
        FlipRot { rotation, flip }
    }
}

impl From<FlipRot> for u8 {
    fn from(flip_rot: FlipRot) -> Self {
        flip_rot.rotation | ((flip_rot.flip as u8) << 2)
    }
}

#[aoc_generator(day20)]
fn parse_input(input: &str) -> Vec<(u16, Tile)> {
    input
        .split("\n\n")
        .map(|tile_str| {
            let tile_lines = tile_str.lines().collect::<Vec<_>>();
            let id_line = &tile_lines[0];
            let id_word = id_line.split_whitespace().nth(1).unwrap();
            let id = id_word[..id_word.len() - 1].parse::<u16>().unwrap();
            let pixels = tile_lines[1..]
                .iter()
                .map(|l| {
                    l.bytes()
                        .map(|b| match b {
                            b'.' => 0,
                            b'#' => 1,
                            _ => unreachable!(),
                        })
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>();
            (id, Tile(pixels))
        })
        .collect::<Vec<_>>()
}

#[aoc(day20, part1)]
fn solve_part1(tiles: &Vec<(u16, Tile)>) -> u64 {
    let remaining = tiles.iter().map(|&(id, _)| id).collect::<Vec<_>>();
    if let Some(placed) = place_tiles(tiles, &Vec::new(), &remaining) {
        let tile_ids =
            placed.iter().map(|&(id, _)| id as u64).collect::<Vec<_>>();
        tile_ids[0]
            * tile_ids[N - 1]
            * tile_ids[(N - 1) * N]
            * tile_ids[(N - 1) * N + N - 1]
    } else {
        0
    }
}

#[aoc(day20, part2)]
fn solve_part2(tiles: &Vec<(u16, Tile)>) -> usize {
    let remaining = tiles.iter().map(|&(id, _)| id).collect::<Vec<_>>();
    if let Some(placed) = place_tiles(tiles, &Vec::new(), &remaining) {
        let borderless_tiles = placed
            .iter()
            .map(|&(id, flip_rot)| {
                let (_, tile) =
                    tiles.iter().find(|(tid, _)| id == *tid).unwrap();
                tile.without_border(flip_rot)
            })
            .collect::<Vec<_>>();
        assert_eq!(borderless_tiles.len(), N * N);
        let tile_size = TILE_SIZE - 2;
        let image_size = tile_size * N;
        let mut image = vec![0; usize::pow(image_size, 2)];
        for tj in 0..N {
            for ti in 0..N {
                // Blit tile in full image.
                let tile = &borderless_tiles[tj * N + ti];
                for (tjj, tile_row) in tile.iter().enumerate() {
                    let idx =
                        ((tj * tile_size) + tjj) * image_size + ti * tile_size;
                    image[idx..idx + tile_size].copy_from_slice(&tile_row);
                }
            }
        }

        let mut image_pixels = Vec::new();
        for row in image.chunks(image_size) {
            image_pixels.push(row.to_vec());
        }

        let sea_monster = "                  # \n\
                           #    ##    ##    ###\n \
                           #  #  #  #  #  #   \n";
        let pattern = sea_monster
            .lines()
            .map(|l| {
                l.bytes()
                    .map(|b| match b {
                        b' ' => 0u8,
                        b'#' => 1u8,
                        _ => unreachable!(),
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        for i in 0..8 {
            let flip_rot = FlipRot::from(i);
            let transformed_image = transform(&image_pixels, flip_rot);

            let mut not_part_of_monster = HashSet::new();
            for y in 0..transformed_image.len() {
                for x in 0..transformed_image[0].len() {
                    if transformed_image[y][x] == 1 {
                        not_part_of_monster.insert((x, y));
                    }
                }
            }

            let mut pattern_found = false;

            for y_offset in 0..=image_size - pattern.len() {
                for x_offset in 0..=image_size - pattern[0].len() {
                    if find_pattern(
                        &transformed_image,
                        &pattern,
                        x_offset,
                        y_offset,
                    ) {
                        for py in 0..pattern.len() {
                            for px in 0..pattern[0].len() {
                                if pattern[py][px] == 1 {
                                    let x = px + x_offset;
                                    let y = py + y_offset;
                                    not_part_of_monster.remove(&(x, y));
                                    pattern_found = true;
                                }
                            }
                        }
                    }
                }
            }

            if pattern_found {
                return not_part_of_monster.len();
            }
        }
    } else {
        panic!("no valid tile placement found");
    }
    0
}

fn find_pattern(
    image: &Vec<Vec<u8>>,
    pattern: &Vec<Vec<u8>>,
    x_offset: usize,
    y_offset: usize,
) -> bool {
    for py in 0..pattern.len() {
        for px in 0..pattern[0].len() {
            let x = px + x_offset;
            let y = py + y_offset;
            if pattern[py][px] == 1 && image[y][x] == 0 {
                return false;
            }
        }
    }
    true
}

// Backtracking algorithm, similar to that used for the N Queen problem.
fn place_tiles(
    tiles: &Vec<(u16, Tile)>,
    placed: &Vec<(u16, FlipRot)>,
    remaining: &Vec<u16>,
) -> Option<Vec<(u16, FlipRot)>> {
    // If there are no more tiles to place, we've found a solution.
    if remaining.is_empty() {
        return Some(placed.clone());
    }
    // Consider the current grid position and try all possible placements of all
    // remaining tiles. Note that for each tile, there are 8 rotations and
    // flips possible. A vertical flip can be represented as a horizontal flip,
    // followed by a 180 degree rotation.
    for ri in 0..remaining.len() {
        let id = remaining[ri];
        let (_, tile) = tiles.iter().find(|(tid, _)| id == *tid).unwrap();
        let mut new_remaining = remaining.clone();
        new_remaining.remove(ri);
        for i in 0..8 {
            let flip_rot = FlipRot::from(i);
            if tile_fits(tiles, placed, tile, flip_rot) {
                let mut new_placed = placed.clone();
                new_placed.push((id, flip_rot));
                if let Some(result) =
                    place_tiles(&tiles, &new_placed, &new_remaining)
                {
                    return Some(result);
                }
            }
        }
    }

    None
}

// Checks whether tile fits with previously placed tiles. Note that we only have
// to consider tiles that are placed to the left of and above the current tile,
// because of the order of the tile handling in the backtracking algorithm.
fn tile_fits(
    tiles: &Vec<(u16, Tile)>,
    placed: &Vec<(u16, FlipRot)>,
    tile: &Tile,
    flip_rot: FlipRot,
) -> bool {
    let row = placed.len() / N;
    let col = placed.len() % N;
    let fits_vertically = if row == 0 {
        true
    } else {
        let p = &placed[(row - 1) * N + col];
        let (_, tile_above) =
            tiles.iter().find(|(tid, _)| p.0 == *tid).unwrap();
        let pixels_above = transform(&tile_above.0, p.1);
        let pixels = transform(&tile.0, flip_rot);
        border(&pixels_above, BorderLocation::Bottom)
            == border(&pixels, BorderLocation::Top)
    };
    let fits_horizontally = if col == 0 {
        true
    } else {
        let p = &placed[row * N + col - 1];
        let (_, tile_on_the_left) =
            tiles.iter().find(|(tid, _)| p.0 == *tid).unwrap();
        let pixels_on_the_left = transform(&tile_on_the_left.0, p.1);
        let pixels = transform(&tile.0, flip_rot);
        border(&pixels_on_the_left, BorderLocation::Right)
            == border(&pixels, BorderLocation::Left)
    };
    fits_vertically && fits_horizontally
}

fn border(pixels: &Vec<Vec<u8>>, loc: BorderLocation) -> u16 {
    let bits = match loc {
        BorderLocation::Top => pixels[0]
            .iter()
            .map(|&n| (n + b'0') as char)
            .collect::<String>(),
        BorderLocation::Left => {
            (pixels.iter().map(|row| row.iter().nth(0).unwrap()))
                .collect::<Vec<_>>()
                .iter()
                .map(|&n| (n + b'0') as char)
                .collect::<String>()
        }
        BorderLocation::Bottom => pixels[pixels.len() - 1]
            .iter()
            .map(|&n| (n + b'0') as char)
            .collect::<String>(),
        BorderLocation::Right => (pixels
            .iter()
            .map(|row| row.iter().nth(pixels[0].len() - 1).unwrap()))
        .collect::<Vec<_>>()
        .iter()
        .map(|&n| (n + b'0') as char)
        .collect::<String>(),
    };
    u16::from_str_radix(&bits, 2).unwrap()
}
