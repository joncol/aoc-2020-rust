//! Day 4: Passport Processing

use std::str::FromStr;

#[derive(Debug, PartialEq)]
enum HeightUnit {
    Centimeters,
    Inches,
}

#[derive(Debug)]
struct Passport {
    // (Birth Year) - four digits; at least 1920 and at most 2002.
    byr: u32,

    // (Issue Year) - four digits; at least 2010 and at most 2020.
    iyr: u32,

    // (Expiration Year) - four digits; at least 2020 and at most 2030.
    eyr: u32,

    // (Height) - a number followed by either cm or in:
    // If cm, the number must be at least 150 and at most 193.
    // If in, the number must be at least 59 and at most 76.
    hgt: (u32, HeightUnit),

    // (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    hcl: String,

    // (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    ecl: String,

    // (Passport ID) - a nine-digit number, including leading zeroes.
    pid: String,
}

impl Passport {
    fn parse(data: &Vec<String>) -> Option<Self> {
        let mut byr = None;
        let mut iyr = None;
        let mut eyr = None;
        let mut hgt = None;
        let mut hcl = None;
        let mut ecl = None;
        let mut pid = None;

        for i in 0..data.len() {
            let pair = data[i].splitn(2, ":").collect::<Vec<_>>();
            let key = pair[0];
            let value = pair[1];
            match key {
                "byr" => byr = u32::from_str(value).ok(),
                "iyr" => iyr = u32::from_str(value).ok(),
                "eyr" => eyr = u32::from_str(value).ok(),
                "hgt" => {
                    let val = u32::from_str(
                        &value
                            .chars()
                            .take_while(|ch| ch.is_digit(10))
                            .collect::<String>(),
                    )
                    .ok()?;
                    let unit = match value
                        .chars()
                        .skip_while(|ch| ch.is_digit(10))
                        .collect::<String>()
                        .as_str()
                    {
                        "cm" => Some(HeightUnit::Centimeters),
                        "in" => Some(HeightUnit::Inches),
                        _ => None,
                    };
                    hgt = unit.map(|u| Some((val, u)))?;
                }
                "hcl" => hcl = Some(String::from(value)),
                "ecl" => ecl = Some(String::from(value)),
                "pid" => pid = Some(String::from(value)),
                _ => {}
            }
        }

        if let (
            Some(byr),
            Some(iyr),
            Some(eyr),
            Some(hgt),
            Some(hcl),
            Some(ecl),
            Some(pid),
        ) = (byr, iyr, eyr, hgt, hcl, ecl, pid)
        {
            if !(1920..=2002).contains(&byr)
                || !(2010..=2020).contains(&iyr)
                || !(2020..=2030).contains(&eyr)
                || (hgt.1 == HeightUnit::Centimeters
                    && !(150..=193).contains(&hgt.0))
                || (hgt.1 == HeightUnit::Inches && !(59..=76).contains(&hgt.0))
                || !hcl.starts_with('#')
                || hcl.len() != 7
                || !hcl.chars().skip(1).all(|ch| ch.is_digit(16))
                || !["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
                    .contains(&&ecl[..])
                || pid.len() != 9
                || !pid.chars().all(|ch| ch.is_digit(10))
            {
                return None;
            }

            Some(Self {
                byr,
                iyr,
                eyr,
                hgt,
                hcl,
                ecl,
                pid,
            })
        } else {
            None
        }
    }
}

#[aoc(day4, part1)]
fn solve_part1(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|s| s.replace('\n', " "))
        .map(|s| {
            s.split(' ')
                .map(|s| s.to_string())
                .filter(|s| !s.starts_with("cid"))
                .collect::<Vec<_>>()
        })
        .filter(|p| p.len() >= 7)
        .count()
}

#[aoc(day4, part2)]
fn solve_part2(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|s| s.replace('\n', " "))
        .map(|s| s.split(' ').map(|s| s.to_string()).collect::<Vec<_>>())
        .filter_map(|v| Passport::parse(&v))
        .count()
}
