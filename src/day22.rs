//! Day 22: Crab Combat

use std::collections::VecDeque;

type Card = u32;

#[derive(Debug, Clone)]
struct Deck(VecDeque<Card>);

impl Deck {
    fn top_card(&mut self) -> Card {
        self.0.pop_front().unwrap()
    }

    fn place_at_bottom(&mut self, cards: &[Card]) {
        for &card in cards.iter() {
            self.0.push_back(card);
        }
    }

    fn score(&self) -> u32 {
        let mut result = 0;
        let mut multiplier = 1;

        for &card in self.0.iter().rev() {
            result += multiplier * card;
            multiplier += 1;
        }

        result
    }

    fn hash(&self) -> String {
        self.0
            .iter()
            .map(|n| n.to_string())
            .collect::<Vec<_>>()
            .join(",")
    }

    fn copy(&self, n: usize) -> Self {
        Self(self.0.iter().copied().take(n).collect())
    }
}

#[derive(Debug)]
enum GameResult {
    PlayerWins,
    CrabWins,
}

#[aoc_generator(day22)]
fn parse_input(input: &str) -> (Deck, Deck) {
    let decks: Vec<VecDeque<Card>> = input
        .split("\n\n")
        .map(|part| part.lines().skip(1).map(|l| l.parse().unwrap()).collect())
        .collect::<Vec<_>>();
    (Deck(decks[0].clone()), Deck(decks[1].clone()))
}

#[aoc(day22, part1)]
fn solve_part1(input: &(Deck, Deck)) -> u32 {
    let mut player_deck = input.0.clone();
    let mut crab_deck = input.1.clone();
    play_game(&mut player_deck, &mut crab_deck);
    player_deck.score().max(crab_deck.score())
}

fn play_game(player_deck: &mut Deck, crab_deck: &mut Deck) {
    while !player_deck.0.is_empty() && !crab_deck.0.is_empty() {
        let player_card = player_deck.top_card();
        let crab_card = crab_deck.top_card();
        assert_ne!(player_card, crab_card);
        if player_card > crab_card {
            player_deck.place_at_bottom(&[player_card, crab_card]);
        } else {
            crab_deck.place_at_bottom(&[crab_card, player_card]);
        }
    }
}

#[aoc(day22, part2)]
fn solve_part2(input: &(Deck, Deck)) -> u32 {
    let mut player_deck = input.0.clone();
    let mut crab_deck = input.1.clone();
    play_recursive(&mut player_deck, &mut crab_deck, &mut Vec::new());
    player_deck.score().max(crab_deck.score())
}

fn play_recursive(
    player_deck: &mut Deck,
    crab_deck: &mut Deck,
    prev_cfgs: &mut Vec<(String, String)>,
) -> GameResult {
    let this_cfg = (player_deck.hash(), crab_deck.hash());
    if prev_cfgs.contains(&this_cfg) {
        return GameResult::PlayerWins;
    } else {
        prev_cfgs.push(this_cfg);
    }

    if player_deck.0.is_empty() {
        return GameResult::CrabWins;
    }

    if crab_deck.0.is_empty() {
        return GameResult::PlayerWins;
    }

    let player_card = player_deck.top_card();
    let crab_card = crab_deck.top_card();

    if player_card <= player_deck.0.len() as u32
        && crab_card <= crab_deck.0.len() as u32
    {
        let mut player_deck_copy = player_deck.copy(player_card as usize);
        let mut crab_deck_copy = crab_deck.copy(crab_card as usize);
        match play_recursive(
            &mut player_deck_copy,
            &mut crab_deck_copy,
            &mut Vec::new(),
        ) {
            GameResult::PlayerWins => {
                player_deck.place_at_bottom(&[player_card, crab_card]);
            }
            GameResult::CrabWins => {
                crab_deck.place_at_bottom(&[crab_card, player_card]);
            }
        }
    } else {
        assert_ne!(player_card, crab_card);
        if player_card > crab_card {
            player_deck.place_at_bottom(&[player_card, crab_card]);
        } else {
            crab_deck.place_at_bottom(&[crab_card, player_card]);
        }
    }
    play_recursive(player_deck, crab_deck, prev_cfgs)
}
