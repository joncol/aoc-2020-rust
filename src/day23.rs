//! Day 23: Crab Cups

#[derive(Debug)]
struct Item {}

#[aoc_generator(day23)]
fn parse_input(input: &str) -> Vec<u32> {
    input
        .lines()
        .nth(0)
        .unwrap()
        .chars()
        .map(|ch| ch.to_digit(10).unwrap())
        .collect::<Vec<u32>>()
}

#[aoc(day23, part1)]
fn solve_part1(input: &Vec<u32>) -> String {
    let cups = input.clone();
    play(cups, 100)
        .take(input.len() - 1)
        .map(|n| n.to_string())
        .collect::<String>()
}

#[aoc(day23, part2)]
fn solve_part2(input: &Vec<u32>) -> u64 {
    let mut cups = input.clone();
    let max = *cups.iter().max().unwrap();
    for c in max + 1..=1_000_000 {
        cups.push(c);
    }
    play(cups, 10_000_000).take(2).map(|n| n as u64).product()
}

// Plays the game and returns an iterator for the cups clockwise of 1, after the
// game is done.
fn play(cups: Vec<u32>, move_count: usize) -> impl Iterator<Item = u32> {
    let min = *cups.iter().min().unwrap();
    let max = *cups.iter().max().unwrap();

    // Create vector of indices to the next cup. For instance, `next_idx[3]` is
    // the index (in `cups`) of the cup immediately clockwise of the cup labeled
    // 3. The vector will be kept up-to-date in the loop below.
    let mut next_idx = vec![0; cups.len() + 1];

    for (i, &c) in cups.iter().enumerate() {
        next_idx[c as usize] = (i as u32 + 1).rem_euclid(cups.len() as u32);
    }

    let mut current_idx = 0;

    for _ in 0..move_count {
        let current_cup = cups[current_idx as usize];

        let mut dest_cup = current_cup - 1;
        if dest_cup < min {
            dest_cup = max;
        }

        let three_cups_idx = next_idx[current_cup as usize];

        // let three_cups =
        //     std::iter::successors(Some(cups[three_cups_idx as usize]), |cup| {
        //         Some(cups[next_idx[*cup as usize] as usize])
        //     })
        //     .take(3)
        //     .collect::<Vec<_>>();

        // The below is a bit faster than the above usage of `successors`

        let cup1 = cups[three_cups_idx as usize];
        let cup2 = cups[next_idx[cup1 as usize] as usize];
        let cup3 = cups[next_idx[cup2 as usize] as usize];
        let three_cups = vec![cup1, cup2, cup3];

        while three_cups.contains(&(dest_cup as u32)) {
            dest_cup -= 1;
            if dest_cup < min {
                dest_cup = max;
            }
        }

        // Reconfigure `next_idx` vector.
        next_idx[current_cup as usize] =
            next_idx[*three_cups.last().unwrap() as usize];
        next_idx[*three_cups.last().unwrap() as usize] =
            next_idx[dest_cup as usize];
        next_idx[dest_cup as usize] = three_cups_idx;

        // Step to next cup.
        current_idx = next_idx[current_cup as usize]
    }

    // Return an iterator for flexible access.
    std::iter::successors(Some(cups[next_idx[1] as usize]), move |cup| {
        Some(cups[next_idx[*cup as usize] as usize])
    })
}
