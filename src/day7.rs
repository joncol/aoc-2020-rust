//! Day 7: Handy Haversacks

use std::collections::{HashMap, HashSet};
use std::str::FromStr;

#[derive(Debug, Clone)]
struct Edge {
    node: String,
    count: usize,
}

type Graph = HashMap<String, Vec<Edge>>;

#[aoc_generator(day7)]
fn parse_input(input: &str) -> (Graph, Graph) {
    let mut graph = HashMap::new();
    let mut rev_graph = HashMap::new();
    for l in input.lines() {
        let container_bag = l
            .split(' ')
            .take(2)
            .map(|s| s.to_string())
            .collect::<Vec<String>>();
        let container_bag = container_bag[0..2].join(" ");

        let contained_bags = l
            .split(' ')
            .skip(4)
            .map(|s| s.to_string())
            .collect::<Vec<String>>();
        let contained_bags = contained_bags
            .join(" ")
            .replace('.', "")
            .replace("no other bags", "")
            .replace("bags", "")
            .replace("bag", "");
        let contained_bags = contained_bags
            .split(", ")
            .map(|s| s.to_string())
            .filter(|s| !s.is_empty())
            .collect::<Vec<String>>();

        for contained in contained_bags {
            let words: Vec<String> =
                contained.split(' ').map(|s| s.to_string()).collect();
            let count = usize::from_str(&words[0]).unwrap();
            let bag: String = words[1..].join(" ").trim().to_string();

            let edge = Edge {
                node: bag.clone(),
                count,
            };
            graph
                .entry(container_bag.clone())
                .or_insert(Vec::new())
                .push(edge);

            let rev_edge = Edge {
                node: container_bag.clone(),
                count,
            };
            rev_graph.entry(bag).or_insert(Vec::new()).push(rev_edge);
        }
    }
    (graph, rev_graph)
}

#[aoc(day7, part1)]
fn solve_part1(graphs: &(Graph, Graph)) -> usize {
    let graph = &graphs.1;
    let mut visited: HashSet<String> = HashSet::new();
    let mut q: Vec<String> = vec![String::from("shiny gold")];
    while !q.is_empty() {
        let next = q.pop().unwrap();
        visited.insert(next.clone());
        let neighbors: Vec<String> =
            graph.get(&next).map_or(Vec::new(), |es| {
                es.iter().map(|e| e.node.clone()).collect()
            });
        for n in neighbors {
            if !q.contains(&n) {
                q.push(n);
            }
        }
    }
    visited.len() - 1
}

#[aoc(day7, part2)]
fn solve_part2(graphs: &(Graph, Graph)) -> usize {
    let graph = &graphs.0;
    let mut q: Vec<(String, usize)> = vec![(String::from("shiny gold"), 1)];
    let mut bag_count = 0;
    while !q.is_empty() {
        let (next, mult) = q.pop().unwrap();
        for n in graph.get(&next).map(|x| x.clone()).unwrap_or(Vec::new()) {
            q.push((n.node, mult * n.count));
            bag_count += n.count * mult;
        }
    }
    bag_count
}
