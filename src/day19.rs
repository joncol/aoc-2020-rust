//! Day 19: Monster Messages

use std::collections::HashMap;

use boolinator::Boolinator;

#[derive(Debug, Clone)]
enum Rule {
    Terminal(char),
    Production(Vec<Vec<usize>>),
}

#[aoc_generator(day19)]
fn parse_input(input: &str) -> (Vec<usize>, HashMap<usize, Rule>, Vec<String>) {
    let sections = input.split("\n\n").collect::<Vec<_>>();

    let mut rule0: Vec<usize> = Vec::new();

    let mut rule_book: HashMap<usize, Rule> = sections[0]
        .lines()
        .map(|l| {
            let parts = l.split(": ").collect::<Vec<_>>();
            let n = parts[0].parse::<usize>().unwrap();

            let rule;

            if n == 0 {
                rule0 = parts[1]
                    .split_whitespace()
                    .map(|s| s.parse().unwrap())
                    .collect::<Vec<_>>();
            }
            if parts[1].starts_with('\"') {
                rule = Rule::Terminal(parts[1].chars().collect::<Vec<_>>()[1]);
            } else {
                let rule_alts = parts[1].split(" | ").collect::<Vec<_>>();
                let ns: Vec<Vec<usize>> = rule_alts
                    .iter()
                    .map(|rule_seq| {
                        rule_seq
                            .split_whitespace()
                            .map(|s| s.parse().unwrap())
                            .collect::<Vec<usize>>()
                    })
                    .collect();
                rule = Rule::Production(ns);
            }
            (n, rule)
        })
        .collect();

    rule_book.remove(&0);

    let messages = sections[1].lines().map(String::from).collect();

    (rule0, rule_book, messages)
}

#[aoc(day19, part1)]
fn solve_part1(
    (rule0, rule_book, messages): &(
        Vec<usize>,
        HashMap<usize, Rule>,
        Vec<String>,
    ),
) -> usize {
    messages
        .iter()
        .filter_map(|msg| {
            is_match(rule_book, rule0, &msg.chars().collect::<Vec<_>>())
                .as_option()
        })
        .count()
}

#[aoc(day19, part2)]
fn solve_part2(
    (rule0, rule_book, messages): &(
        Vec<usize>,
        HashMap<usize, Rule>,
        Vec<String>,
    ),
) -> usize {
    let mut new_rule_book: HashMap<usize, Rule> = (*rule_book).clone();
    new_rule_book.insert(8, Rule::Production(vec![vec![42], vec![42, 8]]));
    new_rule_book
        .insert(11, Rule::Production(vec![vec![42, 31], vec![42, 11, 31]]));
    messages
        .iter()
        .filter_map(|msg| {
            is_match(&new_rule_book, rule0, &msg.chars().collect::<Vec<_>>())
                .as_option()
        })
        .count()
}

fn is_match(
    rule_book: &HashMap<usize, Rule>,
    rules: &[usize],
    input: &[char],
) -> bool {
    if input.is_empty() {
        // If `input` is empty and we've exhausted all the rules, we've got a
        // match.
        return rules.is_empty();
    }

    if rules.is_empty() {
        return false;
    }

    match rule_book.get(&rules[0]).unwrap() {
        Rule::Terminal(ch) => {
            if *ch == input[0] {
                is_match(rule_book, &rules[1..], &input[1..])
            } else {
                false
            }
        }
        Rule::Production(alts) => alts.iter().any(|alt| {
            let mut new_to_match: Vec<usize> = alt.clone();
            new_to_match.extend(&rules[1..]);
            is_match(rule_book, &new_to_match, input)
        }),
    }
}
