//! Day 9: Encoding Error

use std::str::FromStr;

const PREAMBLE_LEN: usize = 25;

#[aoc_generator(day9)]
fn parse_input(input: &str) -> Vec<i64> {
    input.lines().map(|l| i64::from_str(l).unwrap()).collect()
}

#[aoc(day9, part1)]
fn solve_part1(input: &[i64]) -> i64 {
    let mut windows_iter = input.windows(PREAMBLE_LEN);
    let mut window = windows_iter.next().unwrap();
    let mut sum_matrix = construct_sum_matrix(&window);
    let mut result = None;
    for i in PREAMBLE_LEN..input.len() {
        if !sum_matrix.iter().any(|v| v.contains(&input[i])) {
            result = Some(input[i]);
            break;
        }
        // Row and column in triangular matrix to modify.
        let n = i % PREAMBLE_LEN;
        let delta = input[i] - window[0];
        update_tri_matrix(&mut sum_matrix, n, delta);
        window = windows_iter.next().unwrap();
    }
    result.unwrap()
}

fn construct_sum_matrix(values: &[i64]) -> Vec<Vec<i64>> {
    let mut result = Vec::new();
    for row in 0..values.len() {
        let mut sums = Vec::new();
        for column in row..values.len() {
            sums.push(values[row] + values[column]);
        }
        result.push(sums);
    }
    result
}

/// Updates a row and column in the triangular matrix.
fn update_tri_matrix(matrix: &mut [Vec<i64>], n: usize, delta: i64) {
    for i in 0..matrix.len() {
        for j in 0..(matrix.len() - i) {
            if i == n || j == n - i {
                if i == n && j == n - i {
                    matrix[i][j] += delta * 2;
                } else {
                    matrix[i][j] += delta;
                }
            }
        }
    }
}

#[aoc(day9, part2)]
fn solve_part2(input: &[i64]) -> i64 {
    let ans = 530627549; // Answer from part 1.
    let mut i = 0; // Range start (inclusive)
    let mut j = 1; // Range end (inclusive)
    let mut sum = input[i] + input[j];
    loop {
        while sum < ans {
            j += 1;
            sum += input[j];
        }
        if sum == ans {
            break;
        } else {
            sum -= input[i];
            i += 1;

            sum -= input[j];
            j -= 1;
        }
    }
    let range = &input[i..=j];
    range.iter().min().unwrap() + range.iter().max().unwrap()
}
