//! Day 6: Custom Customs

#[aoc_generator(day6)]
fn parse_input(input: &str) -> Vec<Vec<u32>> {
    input
        .split("\n\n")
        .map(|group| {
            group
                .lines()
                .map(|l| {
                    l.as_bytes()
                        .iter()
                        .fold(0, |acc, b| acc | (1 << (b - b'a')) as u32)
                })
                .collect()
        })
        .collect()
}

#[aoc(day6, part1)]
fn solve_part1(input: &[Vec<u32>]) -> u32 {
    input
        .iter()
        .map(|group| group.iter().fold(0, |acc, row| acc | row).count_ones())
        .sum()
}

#[aoc(day6, part2)]
fn solve_part2(input: &[Vec<u32>]) -> u32 {
    input
        .iter()
        .map(|group| {
            group
                .iter()
                .fold(u32::MAX, |acc, row| acc & row)
                .count_ones()
        })
        .sum()
}
