//! Day 13:

#[aoc(day13, part1)]
fn solve_part1(input: &str) -> i32 {
    let lines = input.lines().collect::<Vec<_>>();
    let threshold: i32 = lines[0].parse().unwrap();
    let intervals = lines[1]
        .split(',')
        .filter_map(|w| w.parse::<i32>().ok())
        .collect::<Vec<_>>();
    let min = intervals
        .iter()
        .map(|&n| {
            let wait =
                (threshold as f32 / n as f32).ceil() as i32 * n - threshold;
            (n, wait)
        })
        .min_by(|(_, wait1), (_, wait2)| wait1.cmp(wait2))
        .unwrap();
    min.0 * min.1
}

#[aoc(day13, part2)]
fn solve_part2(input: &str) -> i64 {
    let lines = input.lines().collect::<Vec<_>>();
    let buses: Vec<(i64, i64)> = lines[1]
        .split(',')
        .enumerate()
        .filter_map(|(i, w)| w.parse::<i64>().ok().map(|n| (i as i64, n)))
        .collect::<Vec<_>>();
    let moduli = buses.iter().map(|&(_, b)| b).collect::<Vec<_>>();
    let residues = buses.iter().map(|&(i, b)| b - i).collect::<Vec<_>>();

    match chinese_remainder(&moduli, &residues) {
        Some(rem) => rem,
        None => panic!("moduli are not pairwise coprime"),
    }
}

/// Extended Euclidean Algorithm. Returns the GCD and the factorization
/// `(g, x, y)` such that `a * x + b * y = g`.
fn egcd(a: i64, b: i64) -> (i64, i64, i64) {
    if b == 0 {
        (a, 1, 0)
    } else {
        let (g, x, y) = egcd(b, a % b);
        (g, y, x - y * (a / b))
    }
}

/// Calculates the modular multiplicative inverse of `x` under modulo `m`.
fn mod_inv(x: i64, m: i64) -> Option<i64> {
    let (g, a, _) = egcd(x, m);
    if g == 1 {
        Some(a.rem_euclid(m))
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn egcd_test() {
        assert_eq!(egcd(1180, 482), (2, -29, 71));
        assert_eq!(1180 * (-29) + 482 * 71, 2);
    }

    #[test]
    fn mod_inv_test() {
        assert_eq!(mod_inv(3, 11), Some(4));
        assert_eq!(mod_inv(10, 17), Some(12));
    }
}

fn chinese_remainder(moduli: &[i64], residues: &[i64]) -> Option<i64> {
    let product: i64 = moduli.iter().product();
    let mut sum = 0;
    for (&m, &r) in moduli.iter().zip(residues.iter()) {
        let p = product / m;
        sum += r * mod_inv(p, m)? * p;
    }
    Some(sum % product)
}
