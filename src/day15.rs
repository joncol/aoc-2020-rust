//! Day 15: Rambunctious Recitation

#[aoc_generator(day15)]
fn parse_input(input: &str) -> Vec<u32> {
    input.split(',').map(|w| w.parse().unwrap()).collect()
}

#[aoc(day15, part1)]
fn solve_part1(input: &[u32]) -> u32 {
    solve(&input, 2020)
}

#[aoc(day15, part2)]
fn solve_part2(input: &[u32]) -> u32 {
    solve(&input, 30000000)
}

fn solve(input: &[u32], turn_count: u32) -> u32 {
    // Keep track of which turn a number was last spoken.
    let mut spoken: Vec<u32> =
        vec![0; *input.iter().max().unwrap() as usize + 1];

    for (turn, n) in input.iter().cloned().enumerate() {
        let turn = turn as u32 + 1;
        spoken[n as usize] = turn;
    }

    let mut to_speak = 0;
    let mut last_spoken = 0;

    for turn in input.len() as u32 + 1..=turn_count {
        last_spoken = to_speak;
        if spoken.len() <= to_speak as usize {
            spoken.resize(to_speak as usize + 1, 0);
        }
        if spoken[to_speak as usize] == 0 {
            to_speak = 0;
        } else {
            to_speak = turn - spoken[to_speak as usize];
        }
        spoken[last_spoken as usize] = turn;
    }

    last_spoken
}
