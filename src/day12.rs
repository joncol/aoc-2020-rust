//! Day 12: Rain Risk

#[derive(Debug)]
enum Action {
    North(i32),
    South(i32),
    East(i32),
    West(i32),
    Left(i32),
    Right(i32),
    Forward(i32),
}

#[derive(Debug, Default, Copy, Clone)]
struct Ship {
    x: i32,
    y: i32,
    angle: i32,
}

#[derive(Debug, Copy, Clone)]
struct Waypoint {
    x: i32,
    y: i32,
}

impl Default for Waypoint {
    fn default() -> Self {
        Waypoint { x: 10, y: 1 }
    }
}

#[aoc_generator(day12)]
fn parse_input(input: &str) -> Vec<Action> {
    input
        .lines()
        .map(|l| {
            let n: i32 = l[1..].parse().unwrap();
            match l.as_bytes()[0] {
                b'N' => Action::North(n),
                b'S' => Action::South(n),
                b'E' => Action::East(n),
                b'W' => Action::West(n),
                b'L' => Action::Left(n),
                b'R' => Action::Right(n),
                b'F' => Action::Forward(n),
                _ => unreachable!(),
            }
        })
        .collect()
}

#[aoc(day12, part1)]
fn solve_part1(input: &[Action]) -> i32 {
    let ship = input
        .iter()
        .fold(Ship::default(), |ship, action| do_action(&ship, &action));
    ship.x.abs() + ship.y.abs()
}

fn do_action(ship: &Ship, action: &Action) -> Ship {
    let mut new_ship = ship.clone();
    match action {
        Action::North(dy) => new_ship.y += dy,
        Action::South(dy) => new_ship.y -= dy,
        Action::East(dx) => new_ship.x += dx,
        Action::West(dx) => new_ship.x -= dx,
        Action::Left(angle) => new_ship.angle += angle,
        Action::Right(angle) => new_ship.angle -= angle,
        Action::Forward(l) => {
            let (dx, dy) = rotate_ccw(1, 0, ship.angle);
            new_ship.x += l * dx;
            new_ship.y += l * dy;
        }
    }
    new_ship
}

#[aoc(day12, part2)]
fn solve_part2(input: &[Action]) -> i32 {
    let (ship, _) = input.iter().fold(
        (Ship::default(), Waypoint::default()),
        |(ship, waypoint), action| {
            do_waypoint_action(&ship, &waypoint, &action)
        },
    );
    ship.x.abs() + ship.y.abs()
}

fn do_waypoint_action(
    ship: &Ship,
    waypoint: &Waypoint,
    action: &Action,
) -> (Ship, Waypoint) {
    let mut new_ship = ship.clone();
    let mut new_waypoint = waypoint.clone();
    match action {
        Action::North(dy) => new_waypoint.y += dy,
        Action::South(dy) => new_waypoint.y -= dy,
        Action::East(dx) => new_waypoint.x += dx,
        Action::West(dx) => new_waypoint.x -= dx,
        Action::Left(angle) => {
            let (rx, ry) = rotate_ccw(waypoint.x, waypoint.y, *angle);
            new_waypoint.x = rx;
            new_waypoint.y = ry;
        }
        Action::Right(angle) => {
            let (rx, ry) = rotate_ccw(waypoint.x, waypoint.y, -*angle);
            new_waypoint.x = rx;
            new_waypoint.y = ry;
        }
        Action::Forward(l) => {
            new_ship.x += l * waypoint.x;
            new_ship.y += l * waypoint.y;
        }
    }
    (new_ship, new_waypoint)
}

fn rotate_ccw(x: i32, y: i32, angle: i32) -> (i32, i32) {
    let angle = angle.rem_euclid(360);
    match angle {
        0 => (x, y),
        90 => (-y, x),
        180 => (-x, -y),
        270 => (y, -x),
        _ => panic!("Invalid rotation angle: {}", angle),
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn rotate_ccw_test() {
        assert_eq!(rotate_ccw(1, 0, 90), (0, 1));
        assert_eq!(rotate_ccw(1, 0, 180), (-1, 0));
        assert_eq!(rotate_ccw(1, 0, 270), (0, -1));
        assert_eq!(rotate_ccw(0, 1, 90), (-1, 0));
        assert_eq!(rotate_ccw(0, 1, 180), (0, -1));
        assert_eq!(rotate_ccw(0, 1, 270), (1, 0));
    }
}
