//! Day 8: Handheld Halting

use std::str::FromStr;

#[derive(Debug, Copy, Clone)]
enum Instruction {
    Acc(i32),
    Jmp(i32),
    Nop(i32),
}

impl Instruction {
    fn execute(&self, acc: &mut i32, ip: &mut i32) {
        match self {
            Instruction::Acc(operand) => {
                *acc += operand;
                *ip += 1;
            }
            Instruction::Jmp(operand) => *ip += operand,
            Instruction::Nop(_) => *ip += 1,
        }
    }

    fn flip_nop_jmp(&mut self) {
        match self {
            Instruction::Jmp(operand) => *self = Instruction::Nop(*operand),
            Instruction::Nop(operand) => *self = Instruction::Jmp(*operand),
            _ => {}
        }
    }
}

#[derive(Debug)]
enum RunResult {
    InfiniteLoop(i32),
    Finished(i32),
}

#[aoc_generator(day8)]
fn parse_input(input: &str) -> Vec<Instruction> {
    input
        .lines()
        .map(|l| {
            let tokens: Vec<String> =
                l.split(' ').map(|s| s.to_string()).collect();
            let operand = &tokens[1];
            let operand = i32::from_str(operand).unwrap();
            match tokens[0].as_str() {
                "acc" => Instruction::Acc(operand),
                "jmp" => Instruction::Jmp(operand),
                "nop" => Instruction::Nop(operand),
                _ => unreachable!(),
            }
        })
        .collect()
}

#[aoc(day8, part1)]
fn solve_part1(input: &[Instruction]) -> i32 {
    match run_program(input) {
        RunResult::InfiniteLoop(acc) => acc,
        _ => unreachable!(),
    }
}

#[aoc(day8, part2)]
fn solve_part2(input: &[Instruction]) -> i32 {
    let fix_locations: Vec<_> = input
        .iter()
        .enumerate()
        .filter(|(_, instr)| match instr {
            Instruction::Jmp(_) | Instruction::Nop(_) => true,
            _ => false,
        })
        .map(|(i, _)| i)
        .collect();

    for fix_loc in fix_locations {
        let mut program = input.to_vec();
        program[fix_loc].flip_nop_jmp();
        match run_program(&program) {
            RunResult::Finished(acc) => return acc,
            _ => {}
        }
    }
    unreachable!();
}

fn run_program(program: &[Instruction]) -> RunResult {
    let mut visited: Vec<bool> = vec![false; program.len()];
    let mut acc = 0;
    let mut ip = 0;
    loop {
        if ip as usize == program.len() {
            return RunResult::Finished(acc);
        } else if visited[ip as usize] {
            return RunResult::InfiniteLoop(acc);
        } else {
            visited[ip as usize] = true;
            let instr = &program[ip as usize];
            instr.execute(&mut acc, &mut ip);
        }
    }
}
