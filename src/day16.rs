//! Day 16: Ticket Translation

use std::ops::RangeInclusive;

use boolinator::Boolinator;

#[derive(Debug, Clone)]
struct Field {
    name: String,
    range1: RangeInclusive<u32>,
    range2: RangeInclusive<u32>,
}

impl Field {
    fn is_valid(&self, value: u32) -> bool {
        self.range1.contains(&value) || self.range2.contains(&value)
    }
}

type Ticket = Vec<u32>;

type Data = (Vec<Field>, Ticket, Vec<Ticket>);

#[aoc_generator(day16)]
fn parse_input(input: &str) -> Data {
    let sections = input.split("\n\n").collect::<Vec<_>>();
    let fields = sections[0]
        .lines()
        .map(|l| {
            let parts = l.split(": ").collect::<Vec<_>>();
            let name = String::from(parts[0]);
            let words = parts[1].split(' ').collect::<Vec<_>>();
            let limits1 = words[0]
                .split('-')
                .map(|w| w.parse().unwrap())
                .collect::<Vec<_>>();
            let range1 = RangeInclusive::new(limits1[0], limits1[1]);
            let limits2 = words[2]
                .split('-')
                .map(|w| w.parse().unwrap())
                .collect::<Vec<_>>();
            let range2 = RangeInclusive::new(limits2[0], limits2[1]);
            Field {
                name,
                range1,
                range2,
            }
        })
        .collect();

    let my_ticket = sections[1]
        .lines()
        .skip(1)
        .map(|l| l.split(',').map(|w| w.parse().unwrap()).collect::<Vec<_>>())
        .collect::<Vec<_>>()[0]
        .clone();

    let tickets = sections[2]
        .lines()
        .skip(1)
        .map(|l| l.split(',').map(|w| w.parse().unwrap()).collect::<Vec<_>>())
        .collect();

    (fields, my_ticket, tickets)
}

#[aoc(day16, part1)]
fn solve_part1(input: &Data) -> u32 {
    let (fields, _, tickets) = &input;
    tickets
        .iter()
        .flat_map(|ticket| {
            ticket.iter().filter_map(|&field_value| {
                if !(fields.iter().any(|field| field.is_valid(field_value))) {
                    Some(field_value)
                } else {
                    None
                }
            })
        })
        .sum()
}

#[aoc(day16, part2)]
fn solve_part2(input: &Data) -> u64 {
    let (fields, my_ticket, tickets) = &input;
    let valid_tickets = tickets
        .iter()
        .filter(|ticket| {
            ticket.iter().all(|&field_value| {
                fields.iter().any(|field| field.is_valid(field_value))
            })
        })
        .collect::<Vec<_>>();

    let columns = transpose(&valid_tickets);

    // Convert to numerical matrix of possibility bitmasks.
    let m = fields
        .iter()
        .map(|field| {
            columns
                .iter()
                .map(|column| {
                    if column.iter().all(|&col_val| field.is_valid(col_val)) {
                        '1'
                    } else {
                        '0'
                    }
                })
                .collect::<String>()
        })
        .map(|w| u32::from_str_radix(&w, 2).unwrap())
        .collect::<Vec<_>>();

    let mut with_fields: Vec<(&String, u32)> = fields
        .iter()
        .zip(m.iter())
        .map(|(field, &n)| (&field.name, n))
        .collect::<Vec<_>>();

    // Sort matrix by number of possibilites.
    with_fields
        .sort_by(|(_, n1), (_, n2)| n1.count_ones().cmp(&n2.count_ones()));

    // Do Gaussian elimination.
    for i in 0..with_fields.len() {
        let n = !with_fields[i].1;
        for j in i + 1..with_fields.len() {
            with_fields[j].1 &= n;
        }
    }

    // There should only be one possibility for each.
    for n in &with_fields {
        assert_eq!(n.1.count_ones(), 1);
    }

    with_fields
        .iter()
        .filter_map(|&(field_name, n)| {
            field_name.starts_with("departure").as_some_from(|| {
                // Take into consideration that the bit order is reversed.
                let index = with_fields.len() - 1 - log2(n) as usize;
                my_ticket[index as usize] as u64
            })
        })
        .product()
}

fn transpose<T>(v: &Vec<&Vec<T>>) -> Vec<Vec<T>>
where
    T: Clone,
{
    assert!(!v.is_empty());
    (0..v[0].len())
        .map(|i| v.iter().map(|inner| inner[i].clone()).collect::<Vec<T>>())
        .collect()
}

const fn bit_count<T>() -> usize {
    std::mem::size_of::<T>() * 8
}

fn log2(x: u32) -> u32 {
    assert!(x > 0);
    bit_count::<u32>() as u32 - x.leading_zeros() - 1
}
