//! Day 2: Password Philosophy

#[derive(Debug)]
struct PasswordData {
    policy: (usize, usize),
    letter: char,
    password: String,
}

#[aoc_generator(day2)]
fn parse_int(input: &str) -> Vec<PasswordData> {
    input
        .lines()
        .map(|l| {
            let mut parts = l.split(' ');
            let limits = parts.next().unwrap();
            let mut limits = limits.split('-').map(|s| s.parse().unwrap());
            PasswordData {
                policy: (limits.next().unwrap(), limits.next().unwrap()),
                letter: String::from(parts.next().unwrap())
                    .chars()
                    .next()
                    .unwrap(),
                password: String::from(parts.next().unwrap()),
            }
        })
        .collect()
}

#[aoc(day2, part1)]
fn solve_part1(input: &[PasswordData]) -> usize {
    input
        .iter()
        .map(|p| {
            (
                p.password.chars().filter(|ch| *ch == p.letter).count(),
                p.policy,
            )
        })
        .filter(|(count, policy)| policy.0 <= *count && *count <= policy.1)
        .count()
}

#[aoc(day2, part2)]
fn solve_part2(input: &[PasswordData]) -> usize {
    input
        .iter()
        .map(|p| {
            (
                p.password.chars().nth(p.policy.0 - 1).unwrap(),
                p.password.chars().nth(p.policy.1 - 1).unwrap(),
                p.letter,
            )
        })
        .filter(|(letter1, letter2, ref_letter)| {
            (letter1 == ref_letter) ^ (letter2 == ref_letter)
        })
        .count()
}
