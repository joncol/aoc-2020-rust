//! Day 5: Binary Boarding

#[aoc_generator(day5)]
fn parse_input(input: &str) -> Vec<u16> {
    input
        .lines()
        .map(|l| {
            l.replace('F', "0")
                .replace('B', "1")
                .replace('L', "0")
                .replace('R', "1")
        })
        .map(|l| u16::from_str_radix(&l, 2).unwrap())
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parser_test() {
        let input = "BFFFBBFRRR\n\
                     FFFBBBFRRR\n\
                     BBFFBBFRLL\n";
        let seats = parse_input(input);
        assert_eq!(seats.len(), 3);
        assert_eq!(seats[0], 567);
        assert_eq!(seats[1], 119);
        assert_eq!(seats[2], 820);
    }
}

#[aoc(day5, part1)]
fn solve_part1(input: &[u16]) -> u16 {
    *input.iter().max().unwrap()
}

#[aoc(day5, part2)]
fn solve_part2(input: &[u16]) -> u16 {
    let mut seat_ids: Vec<u16> = input.to_vec();
    seat_ids.sort();

    let mut result = None;
    let mut prev_sid = seat_ids[0];
    let mut it = seat_ids.iter().skip(1).peekable();
    while let Some(sid) = it.next() {
        if let Some(&&next_sid) = it.peek() {
            if !(prev_sid == sid - 1 && next_sid == sid + 1) {
                result = Some(sid + 1);
                break;
            }
        }
        prev_sid = *sid;
    }
    result.unwrap()
}
