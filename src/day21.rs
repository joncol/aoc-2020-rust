//! Day 21: Allergen Assessment

use std::collections::{HashMap, HashSet};

#[derive(Debug)]
struct Item {
    ingredients: Vec<String>,
    allergens: Vec<String>,
}

#[aoc_generator(day21)]
fn parse_input(input: &str) -> Vec<Item> {
    input
        .lines()
        .map(|l| {
            let parts = l.split(" (").collect::<Vec<_>>();
            let ingredients = parts[0]
                .split_whitespace()
                .map(String::from)
                .collect::<Vec<_>>();
            let allergens = parts[1][..parts[1].len() - 1]
                .split_whitespace()
                .skip(1)
                .map(|s| String::from(s.replace(',', "")))
                .collect::<Vec<_>>();
            Item {
                ingredients,
                allergens,
            }
        })
        .collect::<Vec<_>>()
}

#[aoc(day21, part1)]
fn solve_part1(input: &[Item]) -> usize {
    let table = allergen_table(input);

    // Construct a set of all ingredients.
    let mut ingredients = HashSet::new();
    for item in input {
        for ingredient in &item.ingredients {
            ingredients.insert(ingredient.clone());
        }
    }

    // Get allergen-free ingredients.
    let mut allergen_free = ingredients.clone();
    for (_, set) in table {
        for ingredient in set {
            allergen_free.remove(&ingredient);
        }
    }

    input
        .iter()
        .map(|item| {
            item.ingredients
                .iter()
                .filter(|&i| allergen_free.contains(i))
                .count()
        })
        .sum()
}

#[aoc(day21, part2)]
fn solve_part2(input: &[Item]) -> String {
    let mut table = allergen_table(input);
    let mut allergens = HashMap::new();

    // Loop until all allergens have been decided (i.e. there is only one
    // possibility for each allergen).
    while let Some(next) = table.iter().find(|(_, s)| s.len() == 1) {
        let ingredient = next.1.iter().nth(0).unwrap().clone();
        allergens.insert(next.0.clone(), next.1.iter().nth(0).unwrap().clone());

        // Remove the found ingredient from all other allergen sets.
        for (_, set) in &mut table {
            set.remove(&ingredient);
        }
    }

    let mut sorted_keys = allergens.keys().collect::<Vec<_>>();
    sorted_keys.sort();

    sorted_keys
        .iter()
        .map(|&k| allergens.get(k).unwrap())
        .cloned()
        .collect::<Vec<_>>()
        .join(",")
}

fn allergen_table(input: &[Item]) -> HashMap<String, HashSet<String>> {
    // Build up a table of each allergen mapping to the set of possible
    // ingredients that can contain it.
    //
    // Note that each allergen is found in exactly one ingredient, so the set of
    // possible ingredients for an allergen can be found by taking the
    // intersection of all sets where the allergen is guaranteed to be present.

    // First build up a table of each allergen mapping to a vector of sets of
    // ingredients. Later we'll take the intersection of these sets to find the
    // reduced set of ingredients in which the allergen can be present.
    let mut intermediate_table = HashMap::new();

    for item in input {
        for allergen in &item.allergens {
            // Create a set of the ingredients of the item.
            let ingredients =
                item.ingredients.iter().cloned().collect::<HashSet<_>>();

            intermediate_table
                .entry(allergen.clone())
                .or_insert(Vec::new())
                .push(ingredients);
        }
    }

    let mut result = HashMap::new();

    // Calculate the intersection of the sets of ingredients for each allergen.
    for (allergen, sets) in &mut intermediate_table {
        assert!(!sets.is_empty());
        let first_set = sets.pop().unwrap();
        let isect: HashSet<String> = first_set
            .iter()
            .cloned()
            .filter(|k| sets.iter().all(|s| s.contains(k)))
            .collect::<HashSet<_>>();
        result.insert(allergen.clone(), isect);
    }

    result
}
