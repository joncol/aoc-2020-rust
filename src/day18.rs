//! Day 18: Operation Order

use std::{cmp::Ordering, collections::VecDeque};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum Operator {
    Add,
    Mul,
    LeftParens,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum QueueItem {
    Number(u64),
    Operator(Operator),
}

#[aoc(day18, part1)]
fn solve_part1(input: &str) -> u64 {
    input
        .lines()
        .map(|l| evaluate_line(l, |_, _| Ordering::Equal))
        .sum()
}

#[aoc(day18, part2)]
fn solve_part2(input: &str) -> u64 {
    input
        .lines()
        .map(|l| evaluate_line(l, |op1, op2| op2.cmp(op1)))
        .sum()
}

// Parses and evaluates a line using the Shunting Yard Algorithm.
fn evaluate_line<F>(line: &str, precedence_fn: F) -> u64
where
    F: Fn(&Operator, &Operator) -> Ordering,
{
    // Operator stack.
    let mut op_stack: Vec<Operator> = Vec::new();

    // Output queue.
    let mut q = VecDeque::new();

    for ch in line.chars() {
        if ch.is_ascii_digit() {
            let n = ch.to_digit(10).unwrap();
            q.push_back(QueueItem::Number(n as u64));
        } else if ch == '+' || ch == '*' {
            let this_op = match ch {
                '+' => Operator::Add,
                '*' => Operator::Mul,
                _ => unreachable!(),
            };
            loop {
                if op_stack.last() == Some(&Operator::LeftParens) {
                    break;
                }
                // While there's an operator on top of the stack with greater or
                // equal precedence, pop it onto the output queue.
                let o = op_stack.last().map(|op| precedence_fn(op, &this_op));
                if o == Some(Ordering::Greater) || o == Some(Ordering::Equal) {
                    let op = op_stack.pop().unwrap();
                    q.push_back(QueueItem::Operator(op));
                } else {
                    break;
                }
            }
            op_stack.push(this_op);
        } else if ch == '(' {
            op_stack.push(Operator::LeftParens);
        } else if ch == ')' {
            while op_stack.last() != Some(&Operator::LeftParens) {
                let op = op_stack.pop().unwrap();
                q.push_back(QueueItem::Operator(op));
            }
            op_stack.pop();
        }
    }

    // Produce reverse polish notation representation of expression.
    while !op_stack.is_empty() {
        let op = op_stack.pop().unwrap();
        q.push_back(QueueItem::Operator(op));
    }

    // Evaluate RPN.
    let mut evals = Vec::new();
    while !q.is_empty() {
        match q.pop_front().unwrap() {
            QueueItem::Number(n) => evals.push(n),
            QueueItem::Operator(op) => match op {
                Operator::Add => {
                    let op1 = evals.pop().unwrap();
                    let op2 = evals.pop().unwrap();
                    evals.push(op1 + op2);
                }
                Operator::Mul => {
                    let op1 = evals.pop().unwrap();
                    let op2 = evals.pop().unwrap();
                    evals.push(op1 * op2);
                }
                Operator::LeftParens => unreachable!(),
            },
        }
    }

    assert_eq!(evals.len(), 1);

    evals[0]
}
