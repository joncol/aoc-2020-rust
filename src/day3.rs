//! Day 3: Toboggan Trajectory

#[derive(Debug)]
struct Map {
    width: usize,
    height: usize,
    map: Vec<bool>,
}

impl Map {
    fn tree_at_location(&self, x: usize, y: usize) -> bool {
        self.map[y * self.width + (x % self.width)]
    }
}

#[aoc_generator(day3)]
fn parse_int(input: &str) -> Map {
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();
    let mut map = Vec::new();
    input.lines().for_each(|l| {
        l.chars().for_each(|ch| map.push(ch == '#'));
    });
    Map { width, height, map }
}

#[aoc(day3, part1)]
fn solve_part1(input: &Map) -> usize {
    tree_count(input, 3, 1)
}

#[aoc(day3, part2)]
fn solve_part2(input: &Map) -> usize {
    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    slopes
        .iter()
        .map(|&(sx, sy)| tree_count(input, sx, sy))
        .product()
}

fn tree_count(map: &Map, slope_x: usize, slope_y: usize) -> usize {
    (0..)
        .step_by(slope_x)
        .zip((0..map.height).step_by(slope_y))
        .map(|(x, y)| map.tree_at_location(x, y))
        .filter(|&has_tree| has_tree)
        .count()
}
