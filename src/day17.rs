//! Day 17: Conway Cubes

use std::collections::HashSet;

type PointSet = HashSet<(i32, i32, i32, i32)>;

#[derive(Debug, Clone, Copy)]
enum Dimensionality {
    ThreeD,
    FourD,
}

#[aoc_generator(day17)]
fn parse_input(input: &str) -> PointSet {
    input
        .lines()
        .enumerate()
        .flat_map(|(y, l)| {
            l.as_bytes().iter().enumerate().filter_map(move |(x, &ch)| {
                if ch == b'#' {
                    Some((x as i32, y as i32, 0, 0))
                } else {
                    None
                }
            })
        })
        .collect()
}

#[aoc(day17, part1)]
fn solve_part1(input: &PointSet) -> usize {
    let cube = evolutions(input, Dimensionality::ThreeD).nth(6).unwrap();
    cube.len()
}

#[aoc(day17, part2)]
fn solve_part2(input: &PointSet) -> usize {
    let cube = evolutions(input, Dimensionality::FourD).nth(6).unwrap();
    cube.len()
}

fn evolutions(
    cube: &PointSet,
    dim: Dimensionality,
) -> impl Iterator<Item = PointSet> {
    std::iter::successors(Some(cube.clone()), move |cube| {
        Some(evolve(cube, dim.clone()))
    })
}

fn evolve(cube: &PointSet, dim: Dimensionality) -> PointSet {
    let mut new_cube = cube.clone();

    let x1 = cube.iter().map(|(x, _, _, _)| x).min().unwrap();
    let x2 = cube.iter().map(|(x, _, _, _)| x).max().unwrap();
    let y1 = cube.iter().map(|(_, y, _, _)| y).min().unwrap();
    let y2 = cube.iter().map(|(_, y, _, _)| y).max().unwrap();
    let z1 = cube.iter().map(|(_, _, z, _)| z).min().unwrap();
    let z2 = cube.iter().map(|(_, _, z, _)| z).max().unwrap();
    let w_range = match dim {
        Dimensionality::ThreeD => 0..=0,
        Dimensionality::FourD => {
            let w1 = cube.iter().map(|(_, _, _, w)| w).min().unwrap();
            let w2 = cube.iter().map(|(_, _, _, w)| w).max().unwrap();
            w1 - 1..=w2 + 1
        }
    };

    for x in x1 - 1..=x2 + 1 {
        for y in y1 - 1..=y2 + 1 {
            for z in z1 - 1..=z2 + 1 {
                for w in w_range.clone() {
                    let pos = (x, y, z, w);
                    let count = adj_count(&cube, &pos, dim);
                    if cube.contains(&pos) && !(count == 2 || count == 3) {
                        new_cube.remove(&pos);
                    } else if !cube.contains(&pos) && count == 3 {
                        new_cube.insert(pos);
                    }
                }
            }
        }
    }

    new_cube
}

fn adj_count(
    cube: &PointSet,
    pos: &(i32, i32, i32, i32),
    dim: Dimensionality,
) -> usize {
    let mut result = 0;

    let w_range = match dim {
        Dimensionality::ThreeD => 0..=0,
        Dimensionality::FourD => pos.3 - 1..=pos.3 + 1,
    };

    for x in pos.0 - 1..=pos.0 + 1 {
        for y in pos.1 - 1..=pos.1 + 1 {
            for z in pos.2 - 1..=pos.2 + 1 {
                for w in w_range.clone() {
                    if (x, y, z, w) == *pos {
                        continue;
                    }
                    if cube.contains(&(x, y, z, w)) {
                        result += 1;
                    }
                }
            }
        }
    }
    result
}
