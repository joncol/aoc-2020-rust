//! Day 10: Adapter Array

#[aoc_generator(day10)]
fn parse_input(input: &str) -> Vec<i32> {
    let mut adapters: Vec<i32> =
        input.lines().map(|l| l.parse().unwrap()).collect();
    adapters.sort();
    adapters
}

#[aoc(day10, part1)]
fn solve_part1(input: &[i32]) -> i32 {
    let mut prev_adapter = 0;
    let mut one_v_count = 0;
    let mut three_v_count = 0;
    for a in input.iter() {
        if a - prev_adapter == 1 {
            one_v_count += 1;
        } else if a - prev_adapter == 3 {
            three_v_count += 1;
        }
        prev_adapter = *a;
    }
    three_v_count += 1;
    one_v_count * three_v_count
}

#[aoc(day10, part2)]
fn solve_part2(input: &[i32]) -> usize {
    let mut adapters: Vec<i32> = input.to_vec();
    adapters.insert(0, 0);

    let max_value = *adapters.last().unwrap();

    // Keep track of number of paths to each node within reach.
    let mut cache = vec![0; max_value as usize + 1];
    cache[0] = 1;

    for &current in adapters.iter() {
        let neighbors: Vec<&i32> = adapters
            .iter()
            .filter(|&&n| n > current && n <= current + 3)
            .collect();
        for &&n in neighbors.iter() {
            cache[n as usize] += cache[current as usize];
        }
    }
    *cache.iter().last().unwrap()
}

#[aoc(day10, part2, dynamic_programming)]
fn solve_part2_dp(input: &[i32]) -> usize {
    let mut adapters: Vec<i32> = input.to_vec();
    adapters.insert(0, 0);

    let max_value = *adapters.last().unwrap();

    // Keep track of number of ways to reach destination from each node.
    let mut cache = vec![None; max_value as usize + 1];
    cache[adapters.len() - 1] = Some(1);

    fn dp(adapters: &[i32], cache: &mut [Option<usize>], i: usize) -> usize {
        if cache[i].is_some() {
            cache[i].unwrap()
        } else {
            let result = adapters[i + 1..]
                .iter()
                .enumerate()
                .take_while(|(_, &a)| a <= adapters[i] + 3)
                .map(|(j, _)| dp(adapters, cache, j + i + 1))
                .sum();
            cache[i] = Some(result);
            result
        }
    }

    dp(&adapters, &mut cache, 0)
}
