//! Day 1: Report Repair

#[aoc_generator(day1)]
pub fn parse_int(input: &str) -> Vec<i32> {
    input.lines().map(|l| l.parse().unwrap()).collect()
}

/// This is faster than the other variant.
#[aoc(day1, part1, try_for_each)]
pub fn solve_part1_try_for_each(input: &[i32]) -> i32 {
    let result = input
        .iter()
        .try_for_each(|n| match input.iter().find(|&m| m + n == 2020) {
            Some(m) => Err((m, n)),
            None => Ok(()),
        })
        .unwrap_err();

    result.0 * result.1
}

#[aoc(day1, part1, itertools)]
pub fn solve_part1_itertools(input: &[i32]) -> i32 {
    use itertools::Itertools;
    input
        .iter()
        .tuple_combinations()
        .find(|(&x, &y)| x + y == 2020)
        .map(|(x, y)| x * y)
        .unwrap()
}

#[aoc(day1, part2, for_loops)]
pub fn solve_part2_for_loops(input: &[i32]) -> i32 {
    let mut result = (0, 0, 0);
    'outer: for x in input {
        for y in input {
            for z in input {
                if x + y + z == 2020 {
                    result = (*x, *y, *z);
                    break 'outer;
                }
            }
        }
    }
    result.0 * result.1 * result.2
}

/// This is faster than the other variants.
#[aoc(day1, part2, try_for_each)]
pub fn solve_part2_try_for_each(input: &[i32]) -> i32 {
    let result = input
        .iter()
        .try_for_each(|x| {
            input.iter().try_for_each(|y| {
                input.iter().try_for_each(|z| {
                    if x + y + z == 2020 {
                        Err((*x, *y, *z))
                    } else {
                        Ok(())
                    }
                })
            })
        })
        .unwrap_err();
    result.0 * result.1 * result.2
}

#[aoc(day1, part2, itertools)]
pub fn solve_part2_itertools(input: &[i32]) -> i32 {
    use itertools::Itertools;
    input
        .iter()
        .tuple_combinations()
        .find(|(&x, &y, &z)| x + y + z == 2020)
        .map(|(x, y, z)| x * y * z)
        .unwrap()
}
