//! Day 24: Lobby Layout

use std::collections::HashMap;

#[derive(Debug, Clone)]
struct HexGrid(HashMap<(i32, i32), TileColor>);

impl HexGrid {
    fn new(directions: &[Vec<Direction>]) -> HexGrid {
        let mut hm = HashMap::new();

        let coords = directions
            .iter()
            .map(|l| l.iter().fold((0, 0), |(x, y), dir| dir.step(x, y)))
            .collect::<Vec<_>>();

        for &pos in &coords {
            hm.entry(pos).or_insert(TileColor::White).invert();
        }
        HexGrid(hm)
    }

    fn evolutions(&self) -> impl Iterator<Item = Self> {
        std::iter::successors(Some(self.clone()), |grid| Some(grid.evolve()))
    }

    fn evolve(&self) -> Self {
        use TileColor::*;

        let mut new_grid = self.clone();

        let x1 = self.0.keys().min_by_key(|(x, _)| x).unwrap().0 - 1;
        let x2 = self.0.keys().max_by_key(|(x, _)| x).unwrap().0 + 1;
        let y1 = self.0.keys().min_by_key(|(_, y)| y).unwrap().1 - 1;
        let y2 = self.0.keys().max_by_key(|(_, y)| y).unwrap().1 + 1;

        for x in x1..=x2 {
            for y in y1..=y2 {
                let bnc = self.black_neighbor_count(x, y);
                match self.0.get(&(x, y)) {
                    Some(&Black) if bnc == 0 || bnc > 2 => {
                        new_grid.0.insert((x, y), White);
                    }
                    Some(&White) | None if bnc == 2 => {
                        new_grid.0.insert((x, y), Black);
                    }
                    _ => {}
                }
            }
        }

        new_grid
    }

    fn black_neighbor_count(&self, x: i32, y: i32) -> usize {
        use Direction::*;
        use TileColor::*;

        [E, SE, SW, W, NW, NE]
            .iter()
            .filter(|dir| self.0.get(&dir.step(x, y)) == Some(&Black))
            .count()
    }
}

#[derive(Debug)]
enum Direction {
    E,
    SE,
    SW,
    W,
    NW,
    NE,
}

impl Direction {
    fn step(&self, x: i32, y: i32) -> (i32, i32) {
        match self {
            Direction::E => (x + 1, y),
            Direction::SE => (if y % 2 == 0 { x } else { x + 1 }, y + 1),
            Direction::SW => (if y % 2 == 0 { x - 1 } else { x }, y + 1),
            Direction::W => (x - 1, y),
            Direction::NW => (if y % 2 == 0 { x - 1 } else { x }, y - 1),
            Direction::NE => (if y % 2 == 0 { x } else { x + 1 }, y - 1),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum TileColor {
    White,
    Black,
}

impl TileColor {
    fn invert(&mut self) {
        use TileColor::*;

        *self = match self {
            White => Black,
            Black => White,
        };
    }
}

#[aoc_generator(day24)]
fn parse_input(input: &str) -> Vec<Vec<Direction>> {
    use Direction::*;

    let mut directions = Vec::new();

    for l in input.lines() {
        let mut row = Vec::new();
        let mut iter = l.chars().peekable();
        while let Some(ch) = iter.next() {
            let dir = match ch {
                'e' => E,
                's' if iter.peek() == Some(&'e') => {
                    iter.next();
                    SE
                }
                's' if iter.peek() == Some(&'w') => {
                    iter.next();
                    SW
                }
                'w' => W,
                'n' if iter.peek() == Some(&'w') => {
                    iter.next();
                    NW
                }
                'n' if iter.peek() == Some(&'e') => {
                    iter.next();
                    NE
                }
                _ => unreachable!("unexpected char: {}", ch),
            };
            row.push(dir);
        }
        directions.push(row);
    }

    directions
}

#[aoc(day24, part1)]
fn solve_part1(input: &[Vec<Direction>]) -> usize {
    let grid = HexGrid::new(input);
    grid.0
        .iter()
        .filter(|(_, &v)| v == TileColor::Black)
        .count()
}

#[aoc(day24, part2)]
fn solve_part2(input: &[Vec<Direction>]) -> usize {
    let grid = HexGrid::new(input);

    grid.evolutions()
        .nth(100)
        .unwrap()
        .0
        .iter()
        .filter(|(_, &v)| v == TileColor::Black)
        .count()
}
