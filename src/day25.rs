//! Day 25: Combo Breaker

#[aoc_generator(day25)]
fn parse_input(input: &str) -> (u64, u64) {
    let values = input
        .lines()
        .map(|l| l.parse().unwrap())
        .collect::<Vec<_>>();
    (values[0], values[1])
}

#[aoc(day25, part1)]
fn solve_part1(&(card_public_key, door_public_key): &(u64, u64)) -> u64 {
    let mut iter = transformations(7).enumerate();

    while let Some((loop_size, n)) = iter.next() {
        if card_public_key == n {
            return transformations(door_public_key).nth(loop_size).unwrap();
        }
        if door_public_key == n {
            return transformations(card_public_key).nth(loop_size).unwrap();
        }
    }
    unreachable!()
}

fn transformations(subject_number: u64) -> impl Iterator<Item = u64> {
    std::iter::successors(Some(1), move |&value| {
        Some(value * subject_number % 20201227)
    })
}
